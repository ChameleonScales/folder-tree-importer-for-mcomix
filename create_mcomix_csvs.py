#!/bin/python3
import os, sys
from zipfile import ZipFile
from time import strftime, localtime

folder_path = sys.argv[1]
csvs_path = sys.argv[2]
col_id = int(sys.argv[3])
book_id = int(sys.argv[4])

collection_rows = []
collection_names = []
collection_paths = []
book_rows = []
book_paths_ids = {}
contain_rows = []

os.chdir(folder_path)

def dirs_in_cwd():
    dirs = []
    for item in sorted(os.listdir()) :
        if os.path.isdir(item) :
            dirs.append(item)
    return dirs

def zips_in_cwd():
    zips = []
    for item in sorted(os.listdir()) :
        if os.path.splitext(item)[-1] == '.zip' :
            zips.append(item)
    return zips

def count_pages(zip_name):
    with ZipFile(zip_name) as zip:
        contents = zip.namelist()
        count = 0
        for item in contents :
            if not item.endswith('/'):
                count += 1
        return count

def process(parent_id, inside_link, link_target, orig_path):
    global col_id
    global book_id
    global book_rows
    global collection_rows
    global collection_names
    global contain_rows
    parent_id = col_id-1

    parent_dir = os.getcwd()

    if os.path.islink(orig_path):
        inside_link = True
        link_target = os.readlink(orig_path)


    for zip_name in zips_in_cwd():
        path = os.path.abspath(zip_name)
        # if we're inside a linked folder and the same zip file was previously added :
        if inside_link and link_target in collection_paths :
            contain_rows.append([col_id-1, book_paths_ids[path]])
        else :
            pages = count_pages(zip_name)
            format = '0'
            size = os.path.getsize(zip_name)
            date_added = strftime("%Y-%m-%d %H:%M:%S", localtime())
            book_paths_ids[path] = book_id
            book_rows.append([book_id, zip_name, path, pages, format, size, date_added])
            contain_rows.append([col_id-1, book_id])
            book_id += 1

    for dir_name in dirs_in_cwd():
        dupe_count = 0
        col_name = dir_name
        dir_path = os.path.join(os.getcwd(),dir_name)
        # append a number to prevent duplicate collection names
        while col_name in collection_names :
            dupe_count += 1
            col_name = dir_name + '_' + str(dupe_count)
        collection_names.append(col_name)
        collection_paths.append(os.path.abspath(dir_path))
        collection_rows.append([col_id, col_name, parent_id])
        col_id += 1

        os.chdir(dir_path)
        process(parent_id, inside_link, link_target, dir_path)
        os.chdir(parent_dir)


process(col_id, False, None, os.getcwd())

os.chdir(csvs_path)

collections = open("collection.csv", "x")
collections.write("id	name	supercollection\n")
for id, name, parent_id in collection_rows :
    collections.write(f'{id}	{name}	{parent_id}\n')
collections.close()

books = open("book.csv", "x")
books.write("id	name	path	pages	format	size	added\n")
for id, name, path, pages, format, size, date_added in book_rows :
    books.write(f'{id}	{name}	{path}	{pages}	{format}	{size}	{date_added}\n')
books.close()

contain = open("contain.csv", "x")
contain.write("collection	book\n")
for col_id, book_id in contain_rows :
    contain.write(f'{col_id}	{book_id}\n')
contain.close()
