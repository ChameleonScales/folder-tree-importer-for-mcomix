# folder-tree-importer-for-mcomix

A script that generates all the data necessary to import an entire folder tree in mcomix

## Description :

mcomix doesn't allow to import an entire folder tree containing zip archives and preserve its hierarchy in the library. However, its database can be edited in order to achieve that.
This script generates csv files that can be imported in the database through [DB Browser for SQLite](https://sqlitebrowser.org/).
Once imported, the chosen folder tree will be shown as a collection tree in the left panel of the library, along with the zip files contained in each subfolder to use as books.

**Notes:**
* Supports symlinks for zip files as well as folders
* Currently only supports zip archives, though adding support for other formats should be easy

## How to use :

 * put the python file anywhere on your computer
 * in DB Browser for SQLite, open the mcomix database (on linux it should be ~/.local/share/mcomix/library.db)
 * in the Browse Data tab, expand the Table dropdown and choose "book"
 * go to the bottom of the table and take note of the last number in the id column
 * do the same for the collection table
 * the script "create_mcomix_csvs.py" takes 4 arguments:
   * the absolute path to the root folder you want to import in mcomix
   * the absolute path to where you want to export the csv files
   * the collection id to start from (add 1 to the one you took note of)
   * the book id to start from (same technique)
   here's an example :
   `$ ./create_mcomix_csvs.py "/folder/to/import" "folder/to/put/csvs" 49 648`
 * once the csvs are generated, make sure none of the collection names in collection.csv already exists in the SQLite database. Otherwise edit the names in the csv
 * in DB Browser, File > Import > Table from CSV file... : choose the generated collection.csv and use those settings :
   * [x] column names in first line
   * Field separator : tab
 * click ok. When this message appears, say Yes :
   "There is already a table named 'collection'. Do you want to import the data into it?"
 * now do the same for books.csv and then contain.csv
 * open mcomix : the folder structure should now be added to the library.
